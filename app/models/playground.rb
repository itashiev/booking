class Playground < ActiveRecord::Base
  belongs_to :region
  belongs_to :roof
  belongs_to :cover
  belongs_to :sport_type
  has_many :reviews, inverse_of: :playground
  has_many :assets, dependent: :destroy

  validates_associated :region, :roof, :cover, :sport_type
  validates :name, length: {minimum: 1}
  validates :name, presence: true
end

class Cover < ActiveRecord::Base
  validates :name, presence: true
end

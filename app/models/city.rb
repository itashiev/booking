class City < ActiveRecord::Base
  has_many :regions, dependent: :destroy
  has_many :playgrounds

  validates :name, presence: true

end
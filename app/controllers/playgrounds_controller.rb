class PlaygroundsController < ApplicationController
  def index
    @city = City.find(cookies[:city_id].to_i)
    @infrastructures = Infrastructure.all
    @covers = Cover.all
    @playgrounds = @city.playgrounds.page(params[:page]).per(params[:per_page])
  end

  def show
    @playground = Playground.find(params[:id])
  end
end

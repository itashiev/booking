class CitiesController < ApplicationController
  def change
    cookies[:city_id] = {value: params[:city_id]['0'], expires: 1.week.from_now}
    redirect_to :back
  end
end

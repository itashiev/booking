ActiveAdmin.register City do

  form do |f|
    f.inputs 'Details' do
      f.input :name
    end
    f.actions
  end

  permit_params :name
end

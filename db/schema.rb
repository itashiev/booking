# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140816192350) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "assets", force: true do |t|
    t.string   "name"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "playgrounds_id"
  end

  add_index "assets", ["playgrounds_id"], name: "index_assets_on_playgrounds_id", using: :btree

  create_table "cities", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  add_index "cities", ["slug"], name: "index_cities_on_slug", unique: true, using: :btree

  create_table "covers", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "infrastructures", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "infrastructures_playgrounds", id: false, force: true do |t|
    t.integer "playground_id",     null: false
    t.integer "infrastructure_id", null: false
  end

  add_index "infrastructures_playgrounds", ["infrastructure_id", "playground_id"], name: "fk_infrastructure_playground", unique: true, using: :btree

  create_table "play_grounds", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "playgrounds", force: true do |t|
    t.string   "name"
    t.string   "size"
    t.integer  "max_players"
    t.string   "address"
    t.time     "start_time"
    t.time     "end_time"
    t.decimal  "min_price"
    t.text     "description"
    t.integer  "region_id"
    t.integer  "roof_id"
    t.integer  "cover_id"
    t.integer  "sport_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "city_id"
  end

  add_index "playgrounds", ["cover_id"], name: "index_playgrounds_on_cover_id", using: :btree
  add_index "playgrounds", ["region_id"], name: "index_playgrounds_on_region_id", using: :btree
  add_index "playgrounds", ["roof_id"], name: "index_playgrounds_on_roof_id", using: :btree
  add_index "playgrounds", ["sport_type_id"], name: "index_playgrounds_on_sport_type_id", using: :btree

  create_table "regions", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "city_id"
  end

  create_table "reviews", force: true do |t|
    t.text     "cover_description"
    t.text     "general_impression"
    t.text     "infrastructure_description"
    t.integer  "personal_rating"
    t.integer  "field_rating"
    t.integer  "location_rating"
    t.integer  "infrastructure_rating"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "playground_id"
  end

  create_table "roofs", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sport_types", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sport_types_playground", id: false, force: true do |t|
    t.integer "sport_type_id"
    t.integer "playground_id"
  end

  add_index "sport_types_playground", ["sport_type_id", "playground_id"], name: "index_sport_types_playground_on_sport_type_id_and_playground_id", using: :btree

end

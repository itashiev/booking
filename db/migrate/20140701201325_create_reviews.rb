class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.text :personalDescription
      t.text :fieldDescription
      t.text :locationDescription
      t.text :infrastructureDescription
      t.integer :personalRating
      t.integer :fieldRating
      t.integer :locationRating
      t.integer :infrastructureRating

      t.timestamps
    end
  end
end

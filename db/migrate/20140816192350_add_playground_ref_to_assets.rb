class AddPlaygroundRefToAssets < ActiveRecord::Migration
  def change
    add_reference :assets, :playgrounds, index: true
  end
end

class RemoveFileUpdatedAtFromAssets < ActiveRecord::Migration
  def change
    remove_column :assets, :file_updated_at
  end
end

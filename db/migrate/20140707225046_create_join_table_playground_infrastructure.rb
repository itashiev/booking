class CreateJoinTablePlaygroundInfrastructure < ActiveRecord::Migration
  def change
    create_join_table :playgrounds, :infrastructures do |t|
      #t.index [:playground_id, :infrastructure_id]
      t.index [:infrastructure_id, :playground_id], unique: true, name: 'fk_infrastructure_playground'
    end
  end
end
